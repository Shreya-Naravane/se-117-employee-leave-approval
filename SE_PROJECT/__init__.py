from flask import Flask, flash, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from os import path
from flask_login import LoginManager
from werkzeug.utils import secure_filename
import os
from flask_mail import Mail, Message

db = SQLAlchemy()
DB_NAME = "database.db"

UPLOAD_FOLDER = '/static/uploads'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

def create_app():
    app = Flask(__name__)
    #app.config['SECRET_KEY'] = 'hjshjhdjah kjshkjdhjs'
    app.config['SECRET_KEY'] = 'SE-PROJECT-GRP-117'
    app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
    app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
    app.config['MAIL_SERVER']='smtp.gmail.com'
    app.config['MAIL_PORT'] = 465
    app.config['MAIL_USERNAME'] = 'shreya1naravane@gmail.com'
    app.config['MAIL_PASSWORD'] = 'SRN#1234'
    app.config['MAIL_USE_TLS'] = False
    app.config['MAIL_USE_SSL'] = True

    db.init_app(app)
    mail = Mail(app)

    from .dashboard import dashboard
    from .start import start
    from .EMP import EMP

    app.register_blueprint(dashboard, url_prefix='/')
    app.register_blueprint(start, url_prefix='/')
    app.register_blueprint(EMP, url_prefix='/')

    from .models import Users, LoginCredentials, Leave, Payroll, Departments

    create_database(app)

    login_manager = LoginManager()
    login_manager.login_view = 'start.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(id):
        return Users.query.get(int(id))

    return app

def create_database(app):
    if not path.exists('SE_PROJECT/' + DB_NAME):
        db.create_all(app=app)
        print('Created Database!')
