from flask import Blueprint, render_template, request, flash, jsonify
from flask_login import login_required, current_user
from .models import Users,LoginCredentials,Leave
from . import db
import json

dashboard = Blueprint("dashboard", __name__)

@dashboard.route('/employees', methods=['GET', 'POST'])
@login_required
def employees():
	return render_template("employees.html", user=current_user)

@dashboard.route('/director', methods=['GET', 'POST'])
@login_required
def director():
	return render_template("director_page.html", user=current_user)

#@dashboard.route('/proofs', methods=['GET', 'POST'])
#@login_required
#def display_proof():
#	return render_template("proof.html", doc)
