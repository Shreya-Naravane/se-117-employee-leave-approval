from . import db
from flask_login import UserMixin
from sqlalchemy.sql import func
from datetime import datetime
from sqlalchemy import insert

#class Note(db.Model):
 #   id = db.Column(db.Integer, primary_key=True)
  #  data = db.Column(db.String(10000))
   # date = db.Column(db.DateTime(timezone=True), default=func.now())
    
class LoginCredentials(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    emp_id=db.Column(db.Integer, unique=True)
    password = db.Column(db.String(150))
    position=db.Column(db.String(20))

class Users(db.Model, UserMixin):
    id=db.Column(db.Integer, primary_key=True)
    first_name=db.Column(db.String(150))
    last_name=db.Column(db.String(150))
    contact_no=db.Column(db.Integer)
    email=db.Column(db.String(150))
    gen=db.Column(db.String(1))
    dob=db.Column(db.String(10))
    emp_id=db.Column(db.Integer, unique=True)
    dept_id=db.Column(db.Integer)
    position=db.Column(db.String(20))

class Leave(db.Model, UserMixin):
    id=db.Column(db.Integer, primary_key=True)
    emp_id=db.Column(db.Integer)
    start_date=db.Column(db.Date)
    end_date=db.Column(db.Date)
    applied_date=db.Column(db.DateTime(timezone=True), default=func.now())
    leave_status=db.Column(db.String(20))
    leave_type=db.Column(db.String(20))
    file_proof=db.Column(db.String(10))
    doc=db.Column(db.LargeBinary)
    mime_type=db.Column(db.String(30))
    pending_leaves=db.Column(db.Integer)
    mssgs=db.Column(db.String(500))
    dept_id=db.Column(db.Integer)

class Payroll(db.Model, UserMixin):
    id=db.Column(db.Integer, primary_key=True)
    emp_id=db.Column(db.Integer, prmary_key=True)
    leave_id=db.Column(db.Integer)
    total_salary=db.Column(db.Float)
    updated_salary=db.Column(db.Float)
    days_leave=db.Column(db.Integer)

class Departments(db.Model, UserMixin):
    id=db.Column(db.Integer)
    dept_id=db.Column(db.Integer, primary_key=True)
    dept_name=db.Column(db.String(30))
    total_emp=db.Column(db.Integer)
    working_emp=db.Column(db.Integer)
    onleave_emp=db.Column(db.Integer)
    
