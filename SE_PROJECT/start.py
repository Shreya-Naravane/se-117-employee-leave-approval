from flask import Blueprint, render_template, request, flash, redirect, url_for ,send_file,Response
from .models import Users, LoginCredentials, Leave, Payroll, Departments
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
from . import db
from flask_login import login_user, login_required, logout_user, current_user
from datetime import datetime, date
import os
from flask import send_from_directory
from io import BytesIO

rows1=[]

start = Blueprint('start', __name__)

@start.route('/',methods=['GET', 'POST'])
def home_page():
    return render_template("home_page.html")

@start.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        emp_id = request.form.get('emp_id')
        password= request.form.get('password')
        
        user = LoginCredentials.query.filter_by(emp_id=emp_id).first()
        if user:
            if check_password_hash(user.password, password):
                login_user(user, remember=True)
                flash('Logged in successfully!', category='success')
                if(emp_id == "99999998"):
                        return redirect(url_for('dashboard.employees', user=current_user))                      
#return render_template("depts.html", user=current_user) 
                else:
                        print("here")
                        return redirect(url_for('dashboard.employees', user=current_user))
                        #return render_template("employees.html", user=current_user)
            else:
                flash('Incorrect password, try again.', category='error')
        else:
            flash('Employee Account does not exist.Please Sign-up!', category='error')

    return render_template("login.html", user=current_user)

@start.route('/home1',methods=['GET', 'POST'])
@login_required
def home_page1():
    return render_template("home_page 1.html", user=current_user)

@start.route('/display_dashboard')
@login_required
def displays():
   return redirect(url_for('dashboard.employees',user=current_user))


@start.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('start.login'))

@start.route('/leave_start')
@login_required
def leave():
    return render_template("leave1.html", user=current_user)

@start.route('/view_proofs')
@login_required
def display_proof():
    #LEAVE=Leave.query.filter_by(emp_id=current_user.emp_id).first()
    #return render_template("image.html", new_leaves=LEAVE, user=current_user)
    try:
        LEAVE = Leave.query.filter_by(emp_id=current_user.emp_id).all()
        file_data = LEAVE[len(LEAVE)-1]
        return send_file(BytesIO(file_data.doc), mimetype=file_data.mime_type, attachment_filename=None, as_attachment=False)
    except ValueError:
        return "NO uploads"

@start.route('/check_proofs/<empid>')
@login_required
def verify_proof(empid):
    print(empid)
    try:
    #LEAVE=Leave.query.filter_by(emp_id=current_user.emp_id).first()
    #return render_template("image.html", new_leaves=LEAVE, user=current_user)
        LEAVE = Leave.query.filter_by(emp_id=empid).all()
        file_data = LEAVE[len(LEAVE)-1]
        return send_file(BytesIO(file_data.doc), mimetype=file_data.mime_type, attachment_filename=None, as_attachment=False)
    except ValueError:
        #pass
        return "NO uploads"


@start.route('/status')
@login_required
def status1():
    LEAVE=Leave.query.filter_by(emp_id=current_user.emp_id).all()
    if(LEAVE):
         LEAVE1=LEAVE[len(LEAVE)-1]
         return render_template("status.html", new_leaves=LEAVE1, user=current_user)
    else:
         return render_template("no_leave.html", user=current_user)

@start.route('/salary_flow')
@login_required
def check_salary():
    SALARY = Payroll.query.filter_by(emp_id=current_user.emp_id).first()
    return render_template("salary.html", new_salary=SALARY, user=current_user)

@start.route('/depts_add')
@login_required
def add_depts():
    return render_template("depts.html", user=current_user)

@start.route('/depts_check' ,methods=['GET', 'POST'])
@login_required
def check_depts():
    new_dept1 = Departments.query.all()
    return render_template("display_depts.html", name=new_dept1, user=current_user)

@start.route('/dept_status' ,methods=['GET', 'POST'])
@login_required
def status_depts():
    new_dept1 = Departments.query.filter_by(dept_id=current_user.dept_id)
    return render_template("display_depts.html", name=new_dept1, user=current_user)

@start.route('/leave_history', methods=['GET','POST'])
@login_required
def view_history():
    leaves = Leave.query.filter_by(emp_id=current_user.emp_id).all()
    return render_template("display_leave_history.html", leave=leaves, user=current_user)

@start.route('/view_leaves' ,methods=['GET', 'POST'])
@login_required
def view_leaves():
    leaves = []
    d_id = current_user.dept_id
    employees = Users.query.filter_by(dept_id=d_id).all()
    print(employees)
    for j in employees:
         if(j.position == "Manager"):
            employees.remove(j)
    for i in employees:
         rows = Leave.query.filter_by(emp_id=i.emp_id).all()
         print( i )
         if(rows):
            print(len(rows))
            rows1 = rows[len(rows)-1]
            leaves.append(rows1)
    print(rows)
    return render_template("table.html", user=current_user, new_rows=leaves)

@start.route('/salary_status', methods=['GET', 'POST'])
@login_required
def salary_status():
    tmp=[]
    employee = Users.query.filter_by(dept_id=current_user.dept_id).all()
    for i in employee:
         SALARY = Payroll.query.filter_by(emp_id=i.emp_id).first()
         tmp.append(SALARY)
    return render_template("payroll_status.html", user=current_user, new_rows=tmp)

@start.route('/view/<emp_id>', methods=['GET', 'POST'])
@login_required
def view1(emp_id):
    leave = Leave.query.filter_by(emp_id=emp_id).all()
    leave1 = leave[len(leave)-1]
    return render_template("view1.html",leave = leave1, user=current_user)


@start.route('/view1/<emp_id>',methods=['GET', 'POST'])
@login_required
def change(emp_id):
    if(emp_id == "display_dashboard"):
         return redirect(url_for('dashboard.employees', user=current_user))
    emp_id1 = request.form.get(str(emp_id))
    status = request.form.get('app')
    print(status)
    #print('id'+ str(emp_id))
    print(emp_id1)
    print(emp_id)
    
    old_leave1 = Leave.query.filter_by(emp_id=emp_id).all()
    old_leave = old_leave1[len(old_leave1)-1]
    
    #new_leave = Leave(emp_id=emp_id,start_date = old_leave.start_date, end_date=old_leave.end_date,leave_type=old_leave.leave_type,leave_status=status ,file_proof = old_leave.file_proof, dept_id=old_leave.dept_id )
    #db.session.delete(old_leave)
    old_leave.leave_status = status
    #db.session.add(old_leave)
    db.session.commit()
    date1 = int(old_leave.start_date.strftime("%d"))
    print(date1)
    date2 = int(old_leave.end_date.strftime("%d"))
    print(date2)
    m1 = int(old_leave.start_date.strftime("%m"))
    print(m1)
    m2 = int(old_leave.end_date.strftime("%m"))
    print(m2)
    if(status == "Approved"):
        today = int((date.today()).strftime("%d"))
        today1 = int((date.today()).strftime("%m"))
        if(date1 == today and m1 == today1):
            print("In if else")
            dept = Departments.query.filter_by(dept_id=old_leave.dept_id).first()
            print(dept)
            print(dept.dept_name)
            emp_count = dept.working_emp
            print(emp_count)
            emp_count = emp_count - 1
            print(emp_count)
            emp1_count = dept.onleave_emp
            print(emp1_count)
            emp1_count = emp1_count + 1
            print(emp1_count)
            dept.working_emp = emp_count
            dept.onleave_emp = emp1_count
            db.session.add(dept)
            db.session.commit()
        if(date2 + 1  == today  and m2 >= today1):
            dept1 = Departments.query.filter_by(dept_id=old_leave.dept_id).first()
            emp_count = dept.working_emp
            emp_count = emp_count + 1
            emp1_count = dept.onleave_emp
            emp1_count = emp1_count - 1
            dept1.working_emp = emp_count
            dept1.onleave_emp = emp1_count
            db.session.add(dept1)
            db.session.commit()
    #elif (status == "Rejected" or status == "Kept-Pending" or status=="pending"):
    salary = 0
    days = 0
    days1 = 0
    updated_salary = 0
    total_salary = 0
    tmp_row= []
    leaves =  Leave.query.filter_by(emp_id=emp_id).all()
    for i in leaves:
         if(i):
             if(i.leave_status == "Approved"):
                  tmp_row.append(i)
    print(tmp_row)
    for i in tmp_row:
         month1 = (i.start_date).strftime("%m")
         month2 = (i.end_date).strftime("%m")
         year1 = (i.start_date).strftime("%Y")
         year2 = (i.end_date).strftime("%Y")
         if(month1 == month2):
             days = (int(i.end_date.strftime("%d")) - int(i.start_date.strftime("%d")) + 1)
         elif(str(month1)== "02" or str(month1) == "04" or str(month1) =="06" or str(month1) =="09" or str(month1) == "11"):
             d1 = 31 - int(i.start_date.strftime("%d"))
             d2 = int(i.end_date.strftime("%d"))
             days = d1+d2
         else:
             d1 = 31 - int(i.start_date.strftime("%d"))
             d1 = d1 + 1
             d2 = int(i.end_date.strftime("%d"))
             days = d1+d2
         days1 = days1 + days
    final_days = days1 - 10
    person = Users.query.filter_by(emp_id=emp_id).first()
    if(person.position == "Employee"):
         total_salary = 50000
    else:
         total_salary =  75000
    if(final_days > 0):
         updated_salary = total_salary - (2000 * final_days)
         entry = Payroll.query.filter_by(emp_id=i.emp_id).first()
         entry.updated_salary = updated_salary
         entry.days_leave = 10 + final_days
         db.session.commit()    
    #db.session.add(new_leave)
    #db.session.commit()
    if(current_user.emp_id == 99999998):
          rows2=[]
          rows1 = Users.query.filter_by(position="Manager").all()
          print(rows1)
          #rows1.reverse()
          for managers in rows1:
               print(managers.emp_id)
               tmp_row = Leave.query.filter_by(emp_id=managers.emp_id).first()
               #return render_template("table1.html", user=current_user, new_row=tmp_row)
               if(tmp_row):
                    print(rows2)
                    rows2.append(tmp_row)
          #rows2.reverse()
          print(rows2)
          return render_template("table.html", user=current_user, new_rows=rows2)
    else:
          leaves = []
          d_id = current_user.dept_id
          employees = Users.query.filter_by(dept_id=d_id).all()
          for j in employees:
               if(j.position == "Manager"):
                    employees.remove(j)
          for i in employees:
               rows = Leave.query.filter_by(emp_id=i.emp_id).all()
               if(rows):
                    rows1 = rows[len(rows)-1]
                    leaves.append(rows1)
                    print(rows)
          return render_template("table.html", user=current_user, new_rows=leaves)
          
          #print(rows)
          #return render_template("table.html", user=current_user, new_rows=rows)

@start.route('/manager_view_leaves' ,methods=['GET', 'POST'])
@login_required
def view_leaves1():
    rows2=[]
    #d_id = current_user.dept_id
    rows1 = Users.query.filter_by(position="Manager").all()
    print(rows1)
    #rows1.reverse()
    for managers in rows1:
          print(managers.emp_id)
          tmp_row = Leave.query.filter_by(emp_id=managers.emp_id).first()
          #tmp_row = tmp_row1[len(tmp_row1)-1]
          #return render_template("table1.html", user=current_user, new_row=tmp_row)
          print(rows2)
          if(tmp_row):
               rows2.append(tmp_row)
    #rows2.reverse()
    print(rows2)
    return render_template("table.html", user=current_user, new_rows=rows2)


@start.route('/forgot_password' ,methods=['GET', 'POST'])
def change_password():
    return render_template("change_pass.html")

@start.route('/set_new_pass' ,methods=['GET', 'POST'])
def set_pass():
    if request.method == 'POST':
        print("HELLO!")
        emp_id = request.form.get('emp_id')
        new_password1 = request.form.get('password1')
        new_password2 = request.form.get('password2')
        
        old_user1 = LoginCredentials.query.filter_by(emp_id=emp_id).first()
        old_user1.password = generate_password_hash(new_password1, method='sha256')
        #old_user2 = Users.query.filter_by(emp_id=emp_id).first()
        #new_user1 = LoginCredentials(emp_id = old_user1.emp_id, password = generate_password_hash(new_password1, method='sha256'))
        #db.session.delete(old_user1)
        db.session.commit()
        #db.session.add(new_user1)
        #db.session.commit()
        #user1 = LoginCredentials.query.filter_by(emp_id=new_user1.emp_id)
        flash("Password set successfully.Please Login with the new credentials" , category='success')
        return redirect(url_for('start.login'))
      
@start.route('/sign-up', methods=['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        first_name = request.form.get('first_name')
        last_name = request.form.get('last_name')
        contact_no = request.form.get('contact_no')
        email = request.form.get('email')
        gen = request.form.get('gen')
        dob = request.form.get('dob')  
        emp_id = request.form.get('emp_id')
        #session['emp_id'] = emp_idad
        dept_id = request.form.get('dept_id')
        position = request.form.get('position')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')

        user = Users.query.filter_by(emp_id=emp_id).first()
        if user:
            flash('Account already exists.Please Login!', category='error')
        elif len(email) < 4:
            flash('Email must be greater than 3 characters.', category='error')
        elif password1 != password2:
            flash('Passwords don\'t match.', category='error')
            return render_template("sign_up.html",user=current_user)
        elif len(password1)<5:
            flash('Password too short.PLease select a strong password!', category='error')
        elif (position== None):
            flash('Enter your designation', category="error")
        elif (position=="Employee" and dept_id==None):
            flash("Please enter your Department!", category='error')
        elif (position=="Manager" and dept_id==None):
            flash("Please enter your Department!", category='error')
        else:
            if(position == "Employee"):
                 tot_salary = 50000
                 up_salary = 50000
            else:
                 tot_salary = 75000
                 up_salary = 75000
            new_user = Users(first_name=first_name, last_name=last_name, contact_no=contact_no,  email=email, gen=gen, dob=dob, emp_id=emp_id,
dept_id=dept_id, position=position)
            new_user1 = LoginCredentials(emp_id=emp_id, position=position,  password=generate_password_hash(password1, method='sha256'))
            pay_salary = Payroll(emp_id = emp_id, total_salary=tot_salary, updated_salary=up_salary, days_leave=0)
            #leaves_entry =  Leave(emp_id = emp_id, pending_leaves = 40)
            db.session.add(new_user)
            db.session.add(new_user1)
            db.session.add(pay_salary)
            #db.session.add(leaves_entry)
            db.session.commit()
            login_user(new_user, remember=True)
            if(position == "Employee" or position == "Manager"):
                flash('Account created!', category='success')
                return redirect(url_for('dashboard.employees'))
            if(position == "Director"):
                return redirect(url_for('dashboard.employees'))
                #return render_template("depts.html", user=current_user) 

    return render_template("sign_up.html", user=current_user)

@start.route('/leave1',methods=['GET', 'POST'])
@login_required
def leave1():
    if request.method == "POST":
       start_date =  datetime.strptime(request.form.get('df'), "%Y-%m-%d").date()
       if(start_date == None):
            flash("Please Enter appropraite dates!", category="error")
            return render_template("leave1.html", user=current_user)
       end_date = datetime.strptime(request.form.get('dt'), "%Y-%m-%d").date()
       if(end_date == None):
            flash("Please Enter appropraite dates!", category="error")
            return render_template("leave1.html", user=current_user)
       #days_leave = int(end_date.strftime("%d")) - int(start_date.strftime("%d"))
       month1 = start_date.strftime("%m")
       print(month1)
       month2 = end_date.strftime("%m")
       print(month2)
       year1 = start_date.strftime("%Y")
       year2 = end_date.strftime("%Y")
       if(year1 != year2):
            flash("Please enter dates within same year!", category="error")
            return render_template("leave1.html", user=current_user)  
       elif(month2 < month1):
            flash("Please Enter appropraite dates!", category="error")
            return render_template("leave1.html", user=current_user)     
       elif(month1 == month2):
            days_leave = (int(end_date.strftime("%d")) - int(start_date.strftime("%d")) + 1)
            if(days_leave < 0):
               flash("Please Enter appropraite dates!", category="error")
               return render_template("leave1.html", user=current_user)
       else:
            if(str(month1)== "02" or str(month1) == "04" or str(month1) =="06" or str(month1) =="09" or str(month1) == "11"):
                 print("in if")
                 d1 = 31 - int(start_date.strftime("%d"))
                 print(d1)
                 d2 = int(end_date.strftime("%d"))
                 print(d2)
                 days_leave = d1+d2
            else:
                 print("in else")
                 d1 = 31 - int(start_date.strftime("%d"))
                 d1 = d1 + 1
                 d2 = int(end_date.strftime("%d"))
                 days_leave = d1+d2
       leave_type = request.form.get('leave_type')
       mssgs = request.form.get('mssgs')
       list_ext = []
       if(leave_type == "Maternity-Leave" or leave_type == "Quarantine-Leave" or leave_type == "Study/Sabbatical-Leave" or leave_type == "Vacation-Leave"):
            print("in ifelse")
            try:
               f = request.files['file']
               print(f.filename.split(".")[1])
               #list_ext=f.filename.split(".")
               if(str((f.filename.split(".")[1]))!="png" and (f.filename.split(".")[1])!="jpg" and (f.filename.split(".")[1])!="pdf" and (f.filename.split(".")[1])!="jpeg"):
                    print("wrong file")
                    flash("Upload an appropriate file for Your type of leave", category='error')
                    return render_template("leave1.html", user=current_user)
               else:
                    f = request.files['file']
                    doc = f.read()
                    mime_type = f.mimetype
               #f.save(secure_filename(f.filename))
               #filename=secure_filename(f.filename)
               #f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename)
               #new_file = FileContents(name=f.filename ,data=f.read())
               file_proof = "/home/hp/my_flask_app/" + f.filename
            except:
               print("in except")
               flash("Upload file", category='error')
               return render_template("leave1.html", user=current_user)
               #return "Upload File" #here add error.html page coz not working
       else:
            try:
               f = request.files['file']
               print(f.filename.split(".")[1])
               #list_ext=f.filename.split(".")
               if(str((f.filename.split(".")[1]))!="png" and (f.filename.split(".")[1])!="jpg" and (f.filename.split(".")[1])!="pdf" and (f.filename.split(".")[1])!="jpeg"):
                    print("wrong file")
                    flash("Upload an appropriate file for Your type of leave", category='error')
                    return render_template("leave1.html", user=current_user)
               else:
                    f = request.files['file']
                    doc = f.read()
                    mime_type = f.mimetype
               file_proof = "/home/hp/my_flask_app/" + f.filename
            except:
               f = open("/home/hp/my_flask_app/SE_PROJECT/templates/no_proof.html","rb+")
               doc = f.read()               
               file_proof="No Document Upload"
               mime_type = "text/html"
               #return "Upload File" #here add error.html page coz not working 

       old_leave = Leave.query.filter_by(emp_id=current_user.emp_id).first()
        #add initial leaves left
       if old_leave:
            leave_count = old_leave.pending_leaves
            pending_leaves = leave_count - days_leave
            old_payroll = Payroll.query.filter_by(emp_id=current_user.emp_id).first()
            old_leave_count = old_payroll.days_leave
            if(old_leave_count!=None):
                 old_payroll.days_leave = old_leave_count
            else:
                 old_leave_count = 0
                 old_payroll.days_leave = old_leave_count
            #old_payroll.days_leave = days_leave + old_leave_count
       	    #old_leave = Leave(emp_id=current_user.emp_id,start_date = start_date, end_date=end_date,leave_type=leave_type,leave_status="pending" ,file_proof = "no")
            #db.session.delete(old_leave)
            #db.session.commit()
       	    new_leave = Leave(emp_id=current_user.emp_id,start_date = start_date, end_date=end_date,leave_type=leave_type,leave_status="pending" ,file_proof = file_proof, pending_leaves = pending_leaves, doc=doc, mime_type=mime_type ,dept_id=current_user.dept_id, mssgs = mssgs)
            #no_of_days = Payroll(emp_id=current_user.emp_id,days_leave = days_leave)
       	    db.session.add(new_leave)
            #db.session.add(old_payroll)
            db.session.commit()
       else:
            #leave_count = .pending_leaves
            pending_leaves = 10
            new_leave = Leave(emp_id=current_user.emp_id,start_date = start_date, end_date=end_date,leave_type=leave_type,leave_status="Un-viewed" ,file_proof = file_proof, pending_leaves = pending_leaves, doc=doc, mime_type=mime_type,dept_id=current_user.dept_id, mssgs=mssgs)
            #no_of_days = Payroll(days_leave = days_leave)
            old_payroll = Payroll.query.filter_by(emp_id=current_user.emp_id).first()
            old_leave_count = old_payroll.days_leave
            if(old_leave_count!=None):
                 old_payroll.days_leave = old_leave_count
            else:
                 old_leave_count = 0
                 old_payroll.days_leave = old_leave_count
       	    db.session.add(new_leave)
            #db.session.add(old_payroll)
            db.session.commit()
       if(new_leave.pending_leaves != None ):
            if(new_leave.pending_leaves < 0):
                 new_leave.pending_leaves = 0
                 db.session.commit()
       #login_user(new_leave, remember=True)
       LEAVE=Leave.query.filter_by(emp_id=current_user.emp_id).all()
       print(LEAVE)
       LEAVE1=LEAVE[len(LEAVE)-1]
       print(LEAVE1)
       #return "Employee-ID is:" + str(current_user.emp_id) + "start-date" + start_date + "end-date" + end_date
    return render_template("status.html", new_leaves=LEAVE1 ,user=current_user)
       #return 'file uploaded successfully'

     

@start.route('/depts',methods=['GET', 'POST'])
@login_required
def enter_departments():
    #return "start"
    if request.method == "POST":
       dept_id = request.form.get("dept_id")
       dept_name =  request.form.get("dept_name")
       total_emp =  request.form.get("total_emp")
       
       new_dept = Departments(dept_id = dept_id , dept_name=dept_name , total_emp=total_emp , working_emp=total_emp , onleave_emp=0)
       db.session.add(new_dept)
       db.session.commit()
       #return "Hello world"
       new_dept1 = Departments.query.all()
       return render_template("display_depts.html", name=new_dept1, user=current_user)

@start.route('/leave_deletion',methods=['GET', 'POST'])
@login_required
def del_1():
       return render_template("del_leaves.html",user = current_user)

@start.route('/del_leaves',methods=['GET', 'POST'])
@login_required
def deletion():
    if request.method == "POST":
       empid=request.form.get('id')
       delete = Leave.query.filter_by(emp_id=empid).first()
       db.session.delete(delete)
       db.session.commit()
       return "Leave Record deleted!"
