from flask import Blueprint, render_template, request, flash, jsonify
from flask_login import login_required, current_user
from .models import Users,LoginCredentials,Leave
from . import db
import json

EMP = Blueprint("EMP", __name__)

@EMP.route('/', methods=['GET', 'POST'])
@login_required
def status():
	print("hiii")
	return render_template("status.html", user=current_user)


