from flask import Blueprint, render_template, request, flash, jsonify
from flask_login import login_required, current_user
from .models import Users,LoginCredentials,Leave
from . import db
import json

Employee = Blueprint("Employee", __name__)

@Employee.route('/', methods=['GET', 'POST'])
@login_required
def status():
	return render_template("status.html", user=current_user)


